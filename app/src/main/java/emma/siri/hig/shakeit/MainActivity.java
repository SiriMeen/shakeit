package emma.siri.hig.shakeit;

/**
 * @author Emma Nylund
 * @date 21.09.15
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;


public class MainActivity extends AppCompatActivity {

    public static final String PREFS_NAME = "MyPrefsFile";
    private Toolbar tool_bar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tool_bar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(tool_bar);
        tool_bar.setLogo(R.mipmap.ic_launcher);


        tutorialDialogShow();

        checkWifi();
    }


    /**
     * Starts the Accelerometer activity
     *
     * @param view Reference to the widget that is clicked
     */

    public void GetAnActivity(View view) {

        Intent intent = new Intent(this,MainAccelerometer.class);
        startActivity(intent);

    }

    /**
     * AlertDialog shows up only the first time  you open the app
     * If the AlertDialog is not shown than it will be shown
     *
     * Code for the AlertDialog borrowed from http://stackoverflow.com/questions/26097513/android-simple-alert-dialog
     * Code for this borrowed from http://stackoverflow.com/questions/5409595/how-do-i-show-an-alert-dialog-only-on-the-first-run-of-my-application
     *
     */

    public void tutorialDialogShow(){

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        boolean dialogShown = settings.getBoolean("dialogShown", false);

        if (!dialogShown) {
            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setTitle(MainActivity.this.getString(R.string.howToUseTitle));
            alertDialog.setMessage(MainActivity.this.getString(R.string.howToUse));
            alertDialog.setIcon(R.mipmap.ic_launcher);
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();

            //If the AlertDialog has been shown, then don't show it
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("dialogShown", true);
            editor.apply();
        }

    }

    /**
     * Checks the wifi if it's turned on, if not it
     * will ask you if you would turn on the wifi
     *
     * Code borrowed from:
     *      http://stackoverflow.com/questions/6593858/checking-wifi-enabled-or-not-in-android
     *      http://stackoverflow.com/questions/18811694/alarm-dialog-for-turn-on-wifi
     */

    public void checkWifi(){
        WifiManager wifi = (WifiManager)getSystemService(Context.WIFI_SERVICE);

        //Checks to see if the wifi is enabled
        if (!wifi.isWifiEnabled()){

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                    this);

            // The Dialog Title
            alertDialog.setTitle(R.string.wifi_title_string);

            // The Dialog Message
            alertDialog.setMessage(R.string.wifi_message_string);

            // The Icon for the Dialog
            alertDialog.setIcon(R.mipmap.ic_launcher);

            // When pushing the yes button it will take you to the phones wifi setting
            alertDialog.setPositiveButton(R.string.yes_wifi_string,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            // Activity transfer to wifi settings
                            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                        }
                    });

            // When pushing the no button it will cancel the dialog
            alertDialog.setNegativeButton(R.string.no_wifi_string,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to invoke NO event

                            dialog.cancel();
                        }
                    });

            // Showing Alert Message
            alertDialog.show();
        }
    }
}
