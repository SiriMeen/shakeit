package emma.siri.hig.shakeit.tests;

import android.test.InstrumentationTestCase;

/**
 * Created by sirim_000 on 28.09.2015.
 */
public class TestClass extends InstrumentationTestCase {
    public void test() throws Exception{
        final int expected = 1;
        final int reality = 5;
        assertEquals(expected, reality);
    }
}
