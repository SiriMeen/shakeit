package emma.siri.hig.shakeit;

/**
 * @author Emma Nylund
 * @date 22.09.15
 */
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;


/**
 * The Accelerometer class code is borrowed from http://jasonmcreynolds.com/?p=388
 */


public class AccelerometerManager implements SensorEventListener{

    /**
     * This class tkeas care of how to register a shake
     * and count the amount of shakes that is done. It
     * will not take in acount shakes or movement that
     * is less than 300ms apart and wil rest the counter
     * after 1 second of no movement or shakes
     *
     */

    private static final float thresholdGravity = 2.7F;
    private static final int shakeSlopTime = 300;
    private static final int shakeCountResetTime = 1000;

    private OnShakeListener sListener;
    private long mShakeTimestamp;
    private int count;




    public void setOnShakeListener(OnShakeListener listener){
        this.sListener = listener;
    }

    public interface OnShakeListener{
        public void onShake(int count);
    }

    /**
     * Called when the accuracy of a sensor has changed
     *
     * @param s The ID of the sensor
     * @param a The new accuracy
     *
     */

    @Override
    public void onAccuracyChanged(Sensor s, int a){
        //Nothing to do here
    }

    /**
     * This method is called when a sensor have changed its values
     *
     * @param event The ID of the sensor
     */

    @Override
    public void onSensorChanged(SensorEvent event){
        //Checks to see if there is a sensor
        if(sListener != null){
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            float gX = x / SensorManager.GRAVITY_EARTH;
            float gY = y / SensorManager.GRAVITY_EARTH;
            float gZ = z / SensorManager.GRAVITY_EARTH;

            // gForce will be close to 1 when there is no movement.
            double gForce = Math.sqrt(gX * gX + gY * gY + gZ * gZ);

            if(gForce > thresholdGravity){
                final long now = System.currentTimeMillis();


                // Don't pay attention to shakes that are closer than 300ms

                if((mShakeTimestamp + shakeSlopTime) > now){
                    return;
                }

                //Reset the count after 1 second of no movement or shakes

                if((mShakeTimestamp + shakeCountResetTime) < now){

                    count = 0;

                }

                mShakeTimestamp = now;
                count++;

                sListener.onShake(count);
            }
        }
    }
}
