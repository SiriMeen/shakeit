package emma.siri.hig.shakeit;

/**
 * @author Emma Nylund
 * @date 22.09.15
 */

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Custom Toolbar code borrowed from http://www.android4devs.com/2014/12/how-to-make-material-design-app.html
 *
 * Accelerometer code borrowed from http://jasonmcreynolds.com/?p=388
 *
 */


public class MainAccelerometer extends AppCompatActivity{

    /**
     * Will make a accelerometer object and get the amount
     * of shakes which is performed on the phone. From here
     * the amount is used to get a random line from the
     * database in JSON format and then to be parsed and
     * set into a textview which will then display it to the user
     *
     * It will also check to see if the app is set onPause and
     * wil then unregistered the sensor so it won't use unnecessary battery
     *
     */

    private TextView textViewJSON;
    private Toolbar tool_bar;


    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private AccelerometerManager mShakeDetector;

    int mCount = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accelerometer_activity);

        tool_bar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(tool_bar);
        textViewJSON = (TextView) findViewById(R.id.textViewJSON);
        tool_bar.setLogo(R.mipmap.ic_launcher);

        //Gets permission for sensor on the phone
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        //Selects a sensor, here the accelerometer
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        mShakeDetector = new AccelerometerManager();
        mShakeDetector.setOnShakeListener(new AccelerometerManager.OnShakeListener() {

            /**
             * What will happen after a shake. It will get the json data
             * based on the amount of shakes the user will shake
             *
             * @param count Here goes the result count from the Accelerometer class
             *
             */
            @Override
            public void onShake(int count) {

                mCount = count;

                if (count > 0) {
                    getJSON();
                }
            }
        });
    }


    //Register the Listener when the Activity is resumed
    public void onResume() {
        super.onResume();
        mSensorManager.registerListener(mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
    }



    //Unregister the Listener when the Activity is paused
    protected void onPause() {
        mSensorManager.unregisterListener(mShakeDetector);
        super.onPause();
    }



    /**
     * Get the JSON data from the database
     * and parse the data before the data will
     * be set in a textview
     *
     * Code borrowed from http://www.simplifiedcoding.net/android-json-tutorial-to-get-data-from-mysql-database/
     */

    private void getJSON(){


        class GetJSON extends AsyncTask<String, Void, String> {

            /**
             *
             * @param params Parameters from the asyncTask is passed down here
             * @return the parsed JSON data
             *
             */

            @Override
            protected String doInBackground(String... params){

                //Puts up a internet connection to get the JSON data

                try {
                    URL url = new URL("http://www.stud.hig.no/~130703/getActivity.php?shake="+mCount);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    StringBuilder sb = new StringBuilder();
                    con.setRequestMethod("GET");
                    con.setDoOutput(false);

                    BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()),8192);


                    String json;

                    //Checks to see if there is some data
                    while((json = reader.readLine())!= null){
                        sb.append(json);
                    }


                    //Here the data is parsed to a JSON Object

                    JSONObject jObject = new JSONObject(sb.toString());

                    String stringActivity = jObject.getString("activity");

                    try{con.disconnect();}catch(Exception e){}
                    try{reader.close();}catch(Exception e){}

                    return stringActivity;

                }catch(Exception e){
                    return null;
                }

            }

            /**
             * Puts the JSON data into a textview
             *
             * @param s The parsed JSON data from doInBackground function
             */

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                textViewJSON.setText(s);
            }
        }
        GetJSON gj = new GetJSON();
        gj.execute();
    }
}
